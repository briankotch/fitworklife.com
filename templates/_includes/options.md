<form id="form_options" class-"form-horizontal" role="form">
<fieldset>

<!-- Form Name -->
<legend>Options</legend>

<!-- Text input-->
<div class="form-group">
  <label for="active_interval" class="col-md-3" control-label">Carpe Saltem Interval</label>
  <div class="col-md-9">
    <input id="active_interval" class="form-control" name="active_interval" type="text" placeholder="25" class="input-small form-control" required="" value={{active_interval}}>
    <p class="help-block">Number of minutes for the active session</p>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label for="rest_interval class="col-md-3">Rest Interval</label>
  <div class="col-md-9">
    <input id="rest_interval" class="form-control"  name="rest_interval" type="text" placeholder="5" class="input-small form-control" required="" value={{rest_interval}}>
    <p class="help-block">Number of minutes for each break</p>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label for="num_long_breaks" class="col-md-3">Sessions until Long Break</label>
  <div class="col-md-9">
    <input id="count_until_long_break" class="form-control" name="count_until_long_break" type="text" placeholder="4" class="input-small" required="" value="{{count_until_long_break}}">
    <p class="help-block">Number of sessions until a long break</p>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label for="long_break_interval" class="col-md-3">Long Break Length</label>
  <div class="col-md-9">
    <input id="long_break_interval" class="form-control" name="long_break_interval" type="text" placeholder="30" class="input-small" required="" value="{{long_break_interval}}">
    <p class="help-block">Number of minutes for a long break</p>
  </div>
</div>
<div class="form-group">
  <div class="col-md-9">
    <button type="button" class="btn btn-success save_options">Save</button>
    <button type="button" class="btn btn-default cancel_options">Cancel</button>
  </div>
</div>
</fieldset>
</form>