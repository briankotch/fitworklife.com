Fit Work Life
===
Have a Fit Work Life by:
Tracking your time.
Taking exercise breaks.
Pursuing Goals.

We use javascript, HTML5 and nodewebkit to deliver time management and goal tracking to all platforms.

Credits
---
Thanks to majodev for the incredible starter template
https://github.com/majodev/requirejs-nodewebkit-template-project.git
