// Configure RequireJS
require.config({
  baseUrl: "src/lib/",
  paths: {
    "underscore": "../../node_modules/lodash/lodash", // registers as _
    "log": "../vendor/loglevel/loglevel.min", // registers as log
    "Poll": "../vendor/polljs/poll.min", // not AMD compliant, hence shimming.
    "bootstrap": "../vendor/bootstrap/bootstrap.min",
    "bootstrap_validator": "../vendor/bootstrap_validator/bootstrapValidator.min",
    "jquery": "../vendor/jquery/jquery.min",
    "mustache": "../vendor/mustache/mustache",
    "numeral": "../vendor/numeral/numeral.min"
  },
  shim: {
    "Poll": { // example shimming of the non AMD-compliant Poll lib
      exports: "Poll",
      init: function () {
        console.log("r_shim: Poll");
      }
    },
    "numeral": {
        exports: "numeral",
    },
    "bootstrap": {
        exports: "bootstrap",
        deps: ["jquery"],
        init: function() {
            console.log("bootstrap shimmed");
        }
    },
    "bootstrap_validator": {
        exports: "bootstrap_validator",
        deps: ["bootstrap"],
        init: function() {
            console.log("bootstrap validator shimmed");
        }
    }
  }
});

// Start the main app logic.
require(["appInitializer"],
  function(appInitializer) {}
);
