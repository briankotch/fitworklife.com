module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            options: {
                browser: true
            },
            src: ['src/lib/**/*.js']
        },
        shell: {
            "mocha-phantomjs": { // also possible, only display dots for test cases: mocha-phantomjs -R dothttp://localhost:8080/testrunner.html
                command: "mocha-phantomjs http://localhost:8080/testrunner.html", // http-server must run on port 8080!
                options: {
                    stdout: true,
                    stderr: true
                }
            },
            "build-almond": {
                command: "node scripts/build-almond.js",
                options: {
                    stdout: true,
                    stderr: true
                }
            },
            "build-almond-dev": {
                command: "node scripts/build-almond-dev.js",
                options: {
                    stdout: true,
                    stderr: true
                }
            }
        },
        copy: {
            "dev": {
                files: [
                    {
                        src:["css/**/*.css"],
                        dest: "build/"
                    },
		    {
			src:['src/workers/*.js'],
			dest: "build/workers/",
			flatten: true,
			expand: true,
		    }
                ]
            },
            "assets": {
                files: [{
                    src: ["assets/**/*.json", "assets/**/*.png", "assets/**/*.mp3",
                          "assets/**/*.ogg", "assets/**/*.m4a", "assets/**/*.eot",
                          "assets/**/*.ttf", "assets/**/*.woff", "assets/**/*.ico"
                         ],
                    dest: "build/"
                }]
            },
            "build-templates": {
                files: [{
                    expand: true,
                    cwd: "build-templates/",
                    src: ["**/*.json", "**/*.html", "**/*.js"],
                    dest: "build/"
                }]
            },
            "legal": {
                files: [{
                    expand: true,
                    cwd: ".",
                    src: ["LICENSE.md"],
                    dest: "build/"
                }]
            }
        },
        uglify: {
            my_target: {
                files: {
                    'build/app.min.js': ['build-templates/app.js']
                }
            }
        },
        cssmin: {
            options: {
                banner: '/*! <%= pkg.name %> v<%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd HH:MM:s") %> */'
            },
            css: {
                src: [
                    "css/app.css"
                ],
                dest: "build/app.min.css"
            }
        },
        watch: {
            jsFiles: {
                files: ["src/lib/**/*.js", "src/spec/**/*.js"],
                tasks: ["shell:build-almond-dev", "test"]
            },
	    templates: {
		files: ["templates/**/*.*"],
		tasks: ["jekyll"]
	    },
        },
        jekyll: {
            options: {
                src: "templates/",
                config: "templates/_config.yml"
            },
            dist: {
                options: {
                    dest: 'build/'
                }
            }
        },
        nodewebkit: {
            options: {
                build_dir: './release', // target
                mac: true,
                win: true,
                linux32: true,
                linux64: true,
                version: '0.10.2',
                credits: "./build/credits.html"
            },
            src: ['./build/**/*'] // source
        }
    });
    grunt.loadNpmTasks("grunt-jekyll");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-shell");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-node-webkit-builder');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.registerTask("test", ['jshint']);
    grunt.registerTask("dev", ['jekyll', 'copy', 'shell:build-almond-dev', "watch"]);
    grunt.registerTask("default", ['dev']); // default runs the tests, needs running http-server
    grunt.registerTask("build", ["jekyll", "shell:build-almond", "uglify", "copy:build-templates", "copy:assets", "cssmin", "copy:legal"]);
    grunt.registerTask("release", ["build", "nodewebkit"]);
    grunt.registerTask("releasebuild", ["nodewebkit"]);
};
