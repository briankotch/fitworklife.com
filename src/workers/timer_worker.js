importScripts('../build.min.js');;
require({
    baseUrl: "../lib"
},["timer"], function(Timer) {
    var timer = new Timer();
    self.onmessage = function(event) {
        console.log(event);
        switch(event.data) {
            case 'start':
            timer.start();
            break;
            case 'pause':
            timer.stop();
            break;
            case 'stop':
            timer.stop();
            break;
        }
    }

    timer.addEventListener('timer', function(event) {
        postMessage({message: 'timer', seconds: event.seconds});
    });
    postMessage({message: "loaded"});
});
