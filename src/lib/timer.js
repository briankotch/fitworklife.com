define(["EventDispatcher"], function (EventDispatcher) {
    "option strict";
    var Timer = function() {
        var instance = this;
        instance.timer = null;
        instance.rate = 30;
        instance.interval = Math.floor(1000/30);
        instance.timeInit = null;
        instance.time_elapsed = 0;
        instance.dispatched_seconds = 0;

        instance.run = function() {
            var seconds = 0;
            instance.timeInit += this.interval;
            instance.time_elapsed = instance.time_elapsed + 1;
            //dispatch an event every second.
            //Could do milliseconds, but it's not necessary
            seconds = Math.floor(instance.time_elapsed / instance.rate);
            if(seconds > instance.dispatched_seconds) {
                instance.dispatched_seconds = seconds;
                instance.dispatchEvent({
                    type: "timer",
                    seconds: seconds
                });
            }
            instance.timer = setTimeout (
                function() { instance.run(); },
                instance.timeInit - (new Date()).getTime()
            );
        };

        instance.start = function() {
            console.log("Timer started.");
            if (instance.timer === null) {
                instance.timeInit = (new Date()).getTime();
                instance.run();
            }
        };

        instance.stop = function() {
            console.log("Timer stopped.");
            instance.time_elapsed = 0;
            instance.dispatched_seconds = 0;
            clearTimeout(instance.timer);
            instance.timer = null;
        };

    };
    EventDispatcher.prototype.apply(Timer.prototype);
    return Timer;
});
