define([], function() {
    "use strict";
    var Local_Storage = function() {
        var instance = this;
        instance.store = function(key, data) {
            localStorage.setItem(key, JSON.stringify(data));
        };
        instance.get = function(key) {
            var data = JSON.parse(localStorage.getItem(key));
            return data;
        };
    };
    return new Local_Storage();
});
