/*global define */
define(["models/options"], function(Options) {
    "use strict";
    var Options_Controller = function() {
        var instance = this;
        instance.options = new Options();
        instance.update = function(values) {
            var keys, i;
            keys = values.keys();
            for(i = 0; i < keys.length; i = i + 1) {
                instance.options.options[keys[i]] = values[keys[i]];
            }
            instance.options.save();
            console.log(instance.options);
        };
    };
    return new Options_Controller();
});
