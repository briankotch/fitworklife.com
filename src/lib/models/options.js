define(["localstorage"], function(Storage) {
    "use strict";
    var Options = function () {
        var instance = this;
        instance.options = {
            "active_interval": 25,
            "rest_interval": 5,
            "count_until_long_break": 4,
            "long_break_interval": 30
        };
        instance.save = function() {
            Storage.store("options", instance.options);
        };
        instance.load = function() {
            console.log("loading");
            var data = Storage.get("options");
            if(data !== null && typeof data === 'object') {
               instance.options = data;
            }
        };
        instance.load();
    };
    return Options;
});
