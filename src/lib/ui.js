
define(["jquery", "router", "bootstrap_validator"],
       function($, Router) {
           "use strict";
           var Ui = function() {
               var instance = this;
               instance.init = function() {
                   console.log("Initializing UI");
		   console.log(Router);
                   Router.Intervals.addEventListener('timer', function(event) {
                       instance.update_timer(event.seconds);
                   });

	           Router.Intervals.addEventListener('active_interval_complete', function(event) {
                       instance.update_sets(event.active_intervals);
                   });

	           Router.Intervals.addEventListener('rest_interval_complete', function() {
		       instance.rest_mode();
	           });
		   $('#button_start_session').contents().find("svg").on('click', function() {
		       instance.toggle_play_controls();
		       Router.Intervals.start();
		   });


		   $('#button_pause_session').contents().find("svg").on('click', function() {
		       Router.Intervals.pause();
		       instance.toggle_play_controls();
		   });


		   $('#button_stop_session').contents().find("svg").on('click', function() {
		       Router.Intervals.stop();
		       instance.update_timer(0);
		       instance.toggle_play_controls();
		   });


		   $('#button_gear').contents().find("svg").on('click', function() {
		       
		   });


               };

	       instance.rest_mode = function() {
	           var logo = document.querySelector('#logo');
	           var logo_rest = document.querySelector('#logo-rest');
	           var appContainer = document.querySelector('#appContainer');
                   var svgs = document.querySelectorAll(".break_mode_target");
                   for(var i = 0; i < svgs.length; i = i + 1) {
                       console.log(svgs[i]);
                       var path = svgs[i].getSVGDocument().querySelector("path");
                       console.log(path.classList);
                       path.classList.add('break_mode');
	           }
                   logo.classList.toggle("offscreen");
	           logo_rest.classList.toggle("offscreen");
	           appContainer.classList.toggle('break_mode');
	       };

               instance.pad_number = function(n, len) {
                   var n_str = n.toString();
                   if (n_str.length >= len) {
                       return n_str;
                   }
                   return (Array[len + 1].join('0') + n).slice(-len);
               };

               instance.update_sets = function(sets) {
                   var text = "Sets: ";
                   text = text + instance.pad_number(sets, 2);
                   $('#sets').html(text);
               };

               instance.update_timer = function(seconds) {
                   var text = "";
                   //minutes
                   text = text + instance.pad_number(Math.floor(seconds / 60), 2);
                   text = text + ":";
                   //seconds
                   text = text + instance.pad_number(seconds, 2);
                   $('#timer').html(text);
               };

               instance.toggle_play_controls = function() {
                   $('#button_start_session').toggleClass("offscreen");
                   $('#button_pause_session').toggleClass("offscreen");
               };

               instance.get_form = function(form_id) {
                   var values = {};
                   var form_data = $(form_id).serializeArray();
                   var index;
                   for(var i = 0; i < form_data.length; i = i + 1) {
                       var data = form_data[index];
                       values[data.name] = data.value;
                   }
                   return values;
               };

               instance.init_options_dialog = function() {
                   console.log("Binding option events");
                   console.log($('.save_options'));
                   $('.save_options').on('click', function() {
                       Router.Options_Controller.update(instance.get_form("#form_options"));
                       instance.toggle_dialog();
                   });
                   $('.cancel_options').on('click', function() {
                       instance.toggle_dialog();
                   });
                   $('#form_options').bootstrapValidator({
                       message: 'This value is not valid',
                       fields: {
                           rest_interval:{
                               message: "Rest interval is not valid",
                               validators: {
                                   between: {
                                       min: 1,
                                       max: 60,
                                       message: 'Rest interval must be between 1 and 60 minutes'
                                   },
                               }
                           }
                       }
                   });
                   instance.toggle_dialog();
               };

               instance.toggle_dialog = function() {
                   $('.overlay-bg').toggleClass("offscreen");
               };
	   };
           return new Ui();
       });
