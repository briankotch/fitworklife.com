/*global define */
define(
    ["EventDispatcher", "controllers/options_controller", "timer"],
    function(EventDispatcher, Options_Controller, Timer) {
        "use strict";
        var Intervals = function() {
            var instance = this;
            instance.timer_worker = new Worker('workers/timer_worker.js');
            //instance.timer = new Timer();
            instance.time_elapsed = 0;
            instance.options = Options_Controller.options.options;
            instance.active_intervals = 0;
	    instance.rest_intervals = 0;
            instance.mode = "active_interval";
            instance.timer_worker.onmessage = function(event) {
                //console.log(event);
                switch(event.data.message) {
                case "loaded":
                    instance.timer_worker.postMessage("hello");
                    break;
                case "timer":
                    var seconds = event.data.seconds;
                    if(instance.mode === "active_interval") {
                        instance.update_active_interval(seconds);
                    }
		    if(instance.mode === "rest_interval") {
			instance.update_rest_interval(seconds);
		    }
                    break;
                }
            };

            instance.update_timer = function(seconds) {
                instance.dispatchEvent({
                    type: "timer",
                    seconds: seconds
                });
            };

            instance.update_active_interval = function(seconds) {
                var minutes = seconds / 60;
                if (minutes >= instance.options.active_interval) {
                    instance.stop();
                    instance.active_intervals = instance.active_intervals + 1;
                    instance.dispatchEvent({
                        type: "active_interval_complete",
                        active_intervals: instance.active_intervals
                    });
		    instance.mode = "rest_interval";
		    instance.start();
                } else {
                    instance.update_timer(seconds);
                }
            };

	    instance.update_rest_interval = function(seconds) {
		var minutes = seconds / 60;
		if(minutes >= instance.options.rest_interval) {
		    instance.stop();
		    instance.rest_intervals = instance.rest_intervals + 1;
		    instance.dispatchEvent({
			type: "rest_interval_complete",
			rest_intervals: instance.rest_intervals
		    });
		} else {
		    instance.update_timer(seconds);
		}
	    };

            instance.start = function() {
                instance.timer_worker.postMessage("start");
		instance.update_rest_interval(2000);
              //  instance.timer.start();

            };
            instance.pause = function() {
                instance.timer_worker.postMessage("pause");
                //instance.timer.stop();
            };
            instance.stop = function() {
                console.log("Interval Stopped");
                instance.timer_worker.postMessage("stop");
                //instance.timer.stop();
            };
        };
        EventDispatcher.prototype.apply(Intervals.prototype);
        return new Intervals();
    });
