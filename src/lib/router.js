define(["controllers/options_controller", "intervals"], function(Options_Controller, Intervals) {
   var Router = function() {
       var instance = this;
       instance.Options_Controller = Options_Controller;
       instance.Intervals = Intervals;
   };
   return new Router();
});
